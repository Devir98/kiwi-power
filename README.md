# Kiwi-Power #

This test project is based on the brief TECH-OfflineTest-271016-1227.pdf which has been added to the project

## Build ##

The project included a gradle wrapper so that it can be built without having to install gradle locally

./gradlew clean build test


## Running ##

The main class Application can be executed and is currently only processing the readings.csv in the src/main/resources directory