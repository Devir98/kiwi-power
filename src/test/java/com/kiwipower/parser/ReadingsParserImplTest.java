package com.kiwipower.parser;

import com.kiwipower.domain.Reading;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.kiwipower.parser.ReadngsParserImpl.dateTimeFormatter;
import static java.math.BigDecimal.valueOf;
import static org.hamcrest.CoreMatchers.is;
import static org.joda.time.DateTime.parse;
import static org.junit.Assert.*;

public class ReadingsParserImplTest {

    private ReadngsParserImpl readingsParser;

    @Before
    public void setup() {
        readingsParser = new ReadngsParserImpl();
    }

    @Test
    public void shouldParseLineOn() throws Exception {

        //Given
        String line = "#2016-10-20 14:40:37:511,50.0681,0,0,0,0,1469463,1463669,1431871,0,1,off$";

        Reading reading = readingsParser.parseLine(line);

        //Then
        assertNotNull(reading);
        assertThat(reading.getDateTime(), is(parse("2016-10-20 14:40:37:511", dateTimeFormatter)));
        assertThat(reading.getFrequency(), is(valueOf(50.0681)));
        assertThat(reading.getPower(), is(valueOf(4365003)));
        assertFalse(reading.isRelayActive());
    }

    @Test
    public void shouldParseLineOff() throws Exception {

        //Given
        String line = "#2016-10-20 14:40:37:511,50.0681,0,0,0,0,1469463,1463669,1431871,0,1,on$";

        Reading reading = readingsParser.parseLine(line);

        //Then
        assertNotNull(reading);
        assertThat(reading.getDateTime(), is(parse("2016-10-20 14:40:37:511", dateTimeFormatter)));
        assertThat(reading.getFrequency(), is(valueOf(50.0681)));
        assertThat(reading.getPower(), is(valueOf(4365003)));
        assertTrue(reading.isRelayActive());
    }

    @Test
    public void shouldParseFile() throws Exception {

        //Given
        List<Reading> records = readingsParser.parseFile(getClass().getResource("/readings.csv").getPath());

        //Then
        assertNotNull(records);
        assertThat(records.size(), is(37160));
    }
}