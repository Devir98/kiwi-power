package com.kiwipower.processor;

import com.kiwipower.domain.Reading;
import com.kiwipower.domain.Results;
import com.kiwipower.parser.ReadngsParserImpl;
import com.kiwipower.processor.events.EventProcessor;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class ReadingsProcessorImplTest {

    private ReadingsProcessorImpl readingsProcessor;
    private EventProcessor eventProcessor;
    private ReadngsParserImpl readingsParser;

    @Before
    public void setup() {
        eventProcessor = new EventProcessor();
        readingsProcessor = new ReadingsProcessorImpl(eventProcessor);
        readingsParser = new ReadngsParserImpl();
    }

    @Test
    public void processReadings() throws Exception {

        //Given
        List<Reading> readings = readingsParser.parseFile(getClass().getResource("/readings.csv").getPath());

        Results results = readingsProcessor.processReadings(readings);

        //Then
        assertNotNull(results);
        assertTrue(results.isPassedTest());
    }

    @Test
    public void shouldFindTestProfileReadings() throws Exception {

        //Given
        List<Reading> readings = readingsParser.parseFile(getClass().getResource("/readings.csv").getPath());

        readings = readingsProcessor.findTestProfileReadings(readings);

        //Then
        assertNotNull(readings);
        assertThat(readings.size(), is(20992));
    }

}