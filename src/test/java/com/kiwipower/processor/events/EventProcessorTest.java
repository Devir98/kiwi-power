package com.kiwipower.processor.events;

import com.kiwipower.domain.Reading;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static com.google.common.collect.Lists.newArrayList;
import static java.math.BigDecimal.TEN;
import static java.math.BigDecimal.valueOf;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class EventProcessorTest {

    private EventProcessor eventProcessor;
    private DateTime now;

    @Before
    public void setup() {
        eventProcessor = new EventProcessor();

        now = new DateTime();
    }

    @Test
    public void findFfrDropEvent() throws Exception {

        //Given

        Reading reading1 = Reading.builder().frequency(valueOf(49.6)).power(TEN).dateTime(now).build();
        Reading reading2 = Reading.builder().power(TEN).dateTime(now.plusMillis(100)).build();
        Reading reading3 = Reading.builder().power(TEN).dateTime(now.plusMillis(200)).build();

        DateTime result = eventProcessor.findTurnDownDuration(newArrayList(reading1, reading2, reading3), now);

        //Then
        assertThat(result, is(now.plusMillis(100)));
    }

    @Test
    public void shouldFindRelayDuration() throws Exception {
        //Given

        Reading reading1 = Reading.builder().power(TEN).dateTime(now).build();
        Reading reading2 = Reading.builder().power(TEN).dateTime(now.plusMillis(100)).build();
        Reading reading3 = Reading.builder().relayActive(true).power(TEN).dateTime(now.plusMillis(200)).build();

        DateTime result = eventProcessor.findTurnDownDuration(newArrayList(reading1, reading2, reading3), now);

        //Then
        assertThat(result, is(now.plusMillis(100)));
    }

    @Test
    public void shouldFindTurnDownDuration() throws Exception {

        //Given

        Reading reading1 = Reading.builder().power(TEN).dateTime(now).build();
        Reading reading2 = Reading.builder().power(TEN).dateTime(now.plusMillis(100)).build();
        Reading reading3 = Reading.builder().power(TEN).dateTime(now.plusMillis(200)).build();

        DateTime result = eventProcessor.findTurnDownDuration(newArrayList(reading1, reading2, reading3), now);

        //Then
        assertThat(result, is(now.plusMillis(100)));
    }

}