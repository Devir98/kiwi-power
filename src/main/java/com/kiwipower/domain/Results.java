package com.kiwipower.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Results {

    private long relaySwitchDurationMillis;
    private long turnDownDurationMillis;
    private boolean passedTest;
}
