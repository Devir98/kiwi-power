package com.kiwipower.domain;

import lombok.Builder;
import lombok.Data;
import org.joda.time.DateTime;

import java.math.BigDecimal;

@Data
@Builder
public class Reading {

    private final DateTime dateTime;
    private final BigDecimal frequency;
    private final BigDecimal power;
    private final boolean relayActive;
}
