package com.kiwipower.processor.events;

import com.google.common.annotations.VisibleForTesting;
import com.kiwipower.domain.Reading;
import com.kiwipower.domain.Results;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.List;

import static java.math.BigDecimal.valueOf;
import static java.util.stream.Collectors.toList;

public class EventProcessor {

    private final long maxTurnDownMillis = 30 * 1000L;
    private final long maxRelayDurationMillis = 400;
    private final BigDecimal minFfr = valueOf(49.7);
    private final Results results = new Results();

    @VisibleForTesting
    public Results processEvents(List<Reading> readings) {

        //TODO this should be refactored to iterate through the readings once
        DateTime ffrDateTime = findFfrDropEvent(readings);
        System.out.println("FfrDateTime: " + ffrDateTime);
        if(ffrDateTime != null ){
            DateTime relayDateTime = findRelayDuration(readings, ffrDateTime);
            System.out.println("relayDateTime: " + relayDateTime);

            if (relayDateTime != null) {
                DateTime turnDownDateTime = findTurnDownDuration(readings, relayDateTime);
                System.out.println("turnDownDateTime: " + turnDownDateTime);


            }
        }

        System.out.println("RelayDuration: " + results.getRelaySwitchDurationMillis() + "ms");
        System.out.println("TurnDownDuration: " + results.getTurnDownDurationMillis() + "ms");

        //TODO decide if this is the best place
        if(results.getRelaySwitchDurationMillis() < maxRelayDurationMillis &&
                results.getTurnDownDurationMillis() < maxTurnDownMillis) {
            results.setPassedTest(true);
            System.out.println("Test was successful");
        }
        else {
            System.out.println("Test failed");
        }

        return results;
    }

    @VisibleForTesting
    DateTime findFfrDropEvent(List<Reading> readings) {
        readings = readings.stream().filter(reading -> reading.getFrequency().compareTo(minFfr) < 0).collect(toList());

        return readings.get(0).getDateTime();
    }

    @VisibleForTesting
    DateTime findRelayDuration(List<Reading> readings, DateTime ffrDrop) {
        readings = readings.stream().filter(reading -> reading.getDateTime().isAfter(ffrDrop)
                && reading.isRelayActive()).collect(toList());

        results.setRelaySwitchDurationMillis(readings.get(0).getDateTime().getMillis() - ffrDrop.getMillis());

        if(!readings.isEmpty()) {
            return readings.get(0).getDateTime();
        } else {
            return null;
        }

    }

    @VisibleForTesting
    DateTime findTurnDownDuration(List<Reading> readings, DateTime relayActive) {

        Reading previous = null;

        for(Reading reading : readings) {
            if(previous!= null) {
                if(previous.getPower().compareTo(reading.getPower()) == 0) {
                    results.setTurnDownDurationMillis(reading.getDateTime().getMillis() - relayActive.getMillis());

                    return reading.getDateTime();
                }
            }

            previous = reading;
        }

        return null;
    }

}
