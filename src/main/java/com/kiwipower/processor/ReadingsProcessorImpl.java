package com.kiwipower.processor;

import com.google.common.annotations.VisibleForTesting;
import com.kiwipower.domain.Reading;
import com.kiwipower.domain.Results;
import com.kiwipower.processor.events.EventProcessor;
import org.joda.time.DateTime;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static java.math.BigDecimal.valueOf;
import static java.util.stream.Collectors.toList;

public class ReadingsProcessorImpl implements ReadingsProcessor{

    private final int maxRunTimeMins = 35;
    private final int maxProfileReadings = 10;
    private EventProcessor eventProcessor;

    public ReadingsProcessorImpl(EventProcessor eventProcessor) {
        this.eventProcessor = eventProcessor;
    }

    @Override
    public Results processReadings(List<Reading> readings) {
        readings = findTestProfileReadings(readings);

        return eventProcessor.processEvents(readings);
    }

    @VisibleForTesting
    List<Reading> findTestProfileReadings(List<Reading> readings) {
        int noOfProfileReadings = 0;

        for(int i = 0; i < readings.size(); i++) {
            Reading reading = readings.get(i);

            if(reading.getFrequency().compareTo(valueOf(49.99)) > 0 && reading.getFrequency().compareTo(valueOf(50.01)) < 0) {
                noOfProfileReadings++;

                if(noOfProfileReadings == maxProfileReadings) {
                    DateTime startDateTime = readings.get(i - (maxProfileReadings - 1)).getDateTime();
                    DateTime endDateTime = readings.get(i - (maxProfileReadings - 1)).getDateTime().plusMinutes(maxRunTimeMins);

                    return readings.stream().filter(reading1 -> reading1.getDateTime().isAfter(startDateTime)
                            && reading1.getDateTime().isBefore(endDateTime)).collect(toList());
                }
            } else{
                noOfProfileReadings = 0;
            }
        }

        return newArrayList();
    }
}
