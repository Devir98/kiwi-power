package com.kiwipower.processor;

import com.kiwipower.domain.Reading;
import com.kiwipower.domain.Results;

import java.util.List;

public interface ReadingsProcessor {

    Results processReadings(List<Reading> readings);
}
