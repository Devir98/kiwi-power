package com.kiwipower;

import com.kiwipower.domain.Reading;
import com.kiwipower.domain.Results;
import com.kiwipower.parser.ReadingsParser;
import com.kiwipower.parser.ReadngsParserImpl;
import com.kiwipower.processor.ReadingsProcessor;
import com.kiwipower.processor.ReadingsProcessorImpl;
import com.kiwipower.processor.events.EventProcessor;

import java.io.IOException;
import java.util.List;

public class Application {

    public static void main(String[] args) throws IOException {

        ReadingsParser readingsParser = new ReadngsParserImpl();

        List<Reading> readings = readingsParser.parseFile("src/main/resources/readings.csv");

        EventProcessor eventProcessor = new EventProcessor();
        ReadingsProcessor readingsProcessor = new ReadingsProcessorImpl(eventProcessor);

        Results results = readingsProcessor.processReadings(readings);
    }
}
