package com.kiwipower.parser;

import com.kiwipower.domain.Reading;

import java.io.IOException;
import java.util.List;

public interface ReadingsParser {

    List<Reading> parseFile(String path) throws IOException;

}
