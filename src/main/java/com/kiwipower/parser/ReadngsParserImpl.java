package com.kiwipower.parser;

import com.google.common.annotations.VisibleForTesting;
import com.kiwipower.domain.Reading;
import com.kiwipower.exception.InvalidFileFormatException;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Stream;

import static com.google.common.base.Charsets.UTF_8;
import static com.google.common.collect.Lists.newArrayList;
import static java.nio.file.Files.lines;
import static java.nio.file.Paths.get;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.BooleanUtils.toBoolean;
import static org.joda.time.format.DateTimeFormat.forPattern;

public class ReadngsParserImpl implements ReadingsParser {

    final static DateTimeFormatter dateTimeFormatter = forPattern("yyyy-MM-dd HH:mm:ss:SSS");

    //Index of fields
    private static final int DATETIME = 0;
    private static final int FREQUENCY = 1;
    private static final int PHASE_1 = 6;
    private static final int PHASE_2 = 7;
    private static final int PHASE_3 = 8;
    private static final int RELAY_STATE = 11;

    public List<Reading> parseFile(String path) throws IOException {

        try (Stream<String> stream = lines(get(path), UTF_8)) {
            return stream.map(this::parseLine).collect(toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return newArrayList();
    }

    @VisibleForTesting
    Reading parseLine(String line) {

        try {
            if (!line.startsWith("#")) {
                throw new InvalidFileFormatException("Incorrect line format, missing start character '#'");
            }

            if (!line.endsWith("$")) {
                throw new InvalidFileFormatException("Incorrect line format, missing end character '£'");
            }

            String[] fields = line.split(",");
            if (fields.length != 12) {
                throw new InvalidFileFormatException("Incorrect line format, incorrect number of fields");
            }

            DateTime dateTime = dateTimeFormatter.parseDateTime(fields[DATETIME].substring(1));
            BigDecimal frequency = new BigDecimal(fields[FREQUENCY]);
            BigDecimal power = new BigDecimal(fields[PHASE_1])
                    .add(new BigDecimal(fields[PHASE_2]))
                    .add(new BigDecimal(fields[PHASE_3]));

            boolean active = toBoolean(fields[RELAY_STATE].substring(0, fields[RELAY_STATE].length() - 1));

            return Reading.builder()
                    .dateTime(dateTime)
                    .frequency(frequency)
                    .power(power)
                    .relayActive(active).build();

        } catch (InvalidFileFormatException e) {
            System.out.println("Skipping invalid line");
        }
        return null;
    }
}
